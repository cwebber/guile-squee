Squee!  Simple PostgeSQL bindings for Guile!
============================================

What's Squee?  Squee is a library for connecting to the excellent
PostgreSQL database using Guile.  It uses Guile's "foreign function
interface" support, which means you don't need to compile
anything... it all happens through the magic of dynamic linking!

The project is released under the GNU Lesser Public License, version 3
or later (as published by the FSF), just like Guile itself.


Using
-----

  ;; Make a connection to the database
  (define conn (connect-to-postgres-paramstring "dbname=sandbox"))

  ;; Select some stuff from the database
  (exec-query conn "SELECT * FROM animals")
  ; -> (("1" "monkey" "Almost a human" "ooh ooh") ("3" "parrot" "pretty noisy" "ba-kwaw"))
  ;; Select, but substitute in a variable
  (exec-query conn "SELECT * FROM animals where NAME = $1" '("monkey"))
  ; -> (("1" "monkey" "Almost a human" "ooh ooh"))

Technically these functions return multiple values, so if you
"receive" the second parameter, you also can get metadata about your
query.

Queries that don't return rows (as in CREATE or DELETE statements)
will simply return #t, so keep that in mind!

That's about it for now, more to come soon.

